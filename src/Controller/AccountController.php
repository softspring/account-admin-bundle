<?php

namespace Softspring\AccountAdminBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Softspring\AccountBundle\Model\Account;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountController extends AbstractController
{
    /**
     * @return Response
     */
    public function search()
    {
        /** @var EntityRepository $repo */
        $repo = $this->getDoctrine()->getRepository($this->getAccountClass());
        $qb = $repo->createQueryBuilder('a');

        $account = $qb->getQuery()->getResult();

        return $this->render('@SfsAccountAdmin/account/search.html.twig', [
            'account' => $account,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $accountClass = $this->getAccountClass();

        /** @var Account $account */
        $account = new $accountClass();

        $form = $this->createForm($this->getParameter('sfs_account_admin.create_form'), $account, ['validation_groups' => ['Default', 'account-create']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($account);
            $em->flush();

            return $this->redirectToRoute('sfs_account_admin_list');
        }

        return $this->render('@SfsAccountAdmin/account/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param         $account
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function update($account, Request $request)
    {
        $account = $this->getDoctrine()->getRepository($this->getAccountClass())->findOneById($account);

        $form = $this->createForm($this->getParameter('sfs_account_admin.update_form'), $account, ['validation_groups' => ['Default', 'account-update']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($account);
            $em->flush();

            return $this->redirectToRoute('sfs_account_admin_list');
        }

        return $this->render('@SfsAccountAdmin/account/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param         $account
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function delete($account, Request $request)
    {
        $account = $this->getDoctrine()->getRepository($this->getAccountClass())->findOneById($account);

        $em = $this->getDoctrine()->getManager();
        $em->remove($account);
        $em->flush();

        return $this->redirectToRoute('sfs_account_admin_list');
    }

    /**
     * @return string
     */
    protected function getAccountClass(): string
    {
        return $this->getParameter('sfs_account.model.account.class');
    }
}