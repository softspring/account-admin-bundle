<?php

namespace Softspring\AccountAdminBundle\DependencyInjection;

use Softspring\AccountAdminBundle\Form\AccountCreateForm;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sfs_user_admin');

        $rootNode
            ->children()
                ->scalarNode('create_form')->defaultValue(AccountCreateForm::class)->end()
                ->scalarNode('update_form')->defaultValue(AccountCreateForm::class)->end()
            ->end();

        return $treeBuilder;
    }
}