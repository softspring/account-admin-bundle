<?php

namespace Softspring\AccountAdminBundle\Form;

use Softspring\AccountBundle\Model\Account;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountCreateForm extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Account::class,
            'translation_domain' => 'sfs_account_admin',
            'label_format' => 'form.account.%name%.label',
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
    }
}